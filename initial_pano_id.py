#!/usr/env python

import sys
import urllib2
from xml.etree.ElementTree import ElementTree, fromstring

if len(sys.argv) != 3:
    exit("need to supply lat and lon")

lat = sys.argv[1]
lon = sys.argv[2]

# yaw north: 0 south: 180 east: 90 west: 270
# pitch up: -90 down: 90
# http://cbk0.google.com/cbk?output=xml&panoid=p-DIQUVaFuGHWxVqpLstbA

try:
    xml = urllib2.urlopen("http://cbk0.google.com/cbk?output=xml&ll=%s,%s"%(lat, lon))
except urllib2.HTTPError as e:
    exit(e.code + " " + e.msg)

doc = fromstring(xml.read())
pano_id = doc.find("data_properties")

if pano_id is None:
    exit("no streetview data avaiable here")

print pano_id.get("pano_id")
