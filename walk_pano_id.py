#!/usr/env python

import sys
import urllib2
from xml.etree.ElementTree import ElementTree, fromstring

def links_from_panoid(panoid):
    try:
        xml = urllib2.urlopen("http://cbk0.google.com/cbk?output=xml&panoid=%s"%panoid)
    except urllib2.HTTPError as e:
        exit(e.code + " " + e.msg)
    doc = fromstring(xml.read())
    return [link.get("pano_id") for link in doc.findall("annotation_properties/link")]

if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("need to supply maximum number of positions")

    from_stdin = sys.stdin.readlines()
    if len(from_stdin) != 1:
        exit("only one initial pano_id expected on stdin")
    init_pano = from_stdin[0].strip()
    max_pos = int(sys.argv[1])

    result = set([init_pano])
    toprocess = [init_pano]

    # breadth first search
    while len(result) < max_pos and toprocess:
        newtoprocess = []
        for panoid in toprocess:
            if len(result) >= max_pos:
                break
            ids = links_from_panoid(panoid)
            for p in ids:
                if p not in result:
                    newtoprocess.append(p)
            result.update(ids)
        toprocess = newtoprocess

    for p in result:
        print p
