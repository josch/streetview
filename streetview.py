#!/usr/env python

import sys
import urllib2
from xml.etree.ElementTree import ElementTree, fromstring
from PIL import Image
from StringIO import StringIO

gridsizes = [(1, 1),
             (2, 1),
             (4, 2),
             (6, 3),
             (13, 7),
             (26, 13)]

# yaw north: 0 south: 180 east: 90 west: 270
# pitch up: -90 down: 90
# http://cbk0.google.com/cbk?output=xml&panoid=p-DIQUVaFuGHWxVqpLstbA

def process(panoid, zoom):
    print >>sys.stderr, panoid
    # retrieve tiles and save panorama
    panorama = Image.new("RGB", (gridsizes[zoom][0]*512, gridsizes[zoom][1]*512))
    try:
        for x in xrange(gridsizes[zoom][0]):
            for y in xrange(gridsizes[zoom][1]):
                print >>sys.stderr, x, y
                tile = urllib2.urlopen("http://cbk0.google.com/cbk?output=tile&panoid=%s&zoom=%d&x=%d&y=%d"%(panoid, zoom, x, y))
                imtile = Image.open(StringIO(tile.read()))
                panorama.paste(imtile, (x*512, y*512))
    except urllib2.HTTPError as e:
        exit(e.code + " " + e.msg)
    panorama.save("%s_panorama.jpg"%panoid)

    # divide into 16 images around the center
    num_imgs = 8

    cwidth = (512*gridsizes[zoom][0])/num_imgs
    cheight = (cwidth*3)/4 # 4:3 aspect
    upper = (gridsizes[zoom][1]*512)/2 - cheight/2
    lower = upper+cheight

    for i in xrange(num_imgs):
        p = panorama.crop((i*cwidth,upper,(i+1)*cwidth,lower))
        filename="%s_img_%003d.jpg"%(panoid,i)
        p.save(filename)

    # save thumbnail
    try:
        url = urllib2.urlopen("http://cbk0.google.com/cbk?output=thumbnail&panoid=%s&w=416&h=208"%panoid)
        open("%s_thumb.jpg"%panoid, "w").write(url.read())
    except urllib2.HTTPError as e:
        exit(e.code + " " + e.msg)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("need to supply zoom (0-5)")

    zoom = int(sys.argv[1])

    from_stdin = sys.stdin.readlines()
    for line in from_stdin:
        process(line.strip(), zoom)



